# Assignment 3 - Numerical Differentiation

Code for MTMW12 Assignment 3 - Numerical Differentiation

To obtain a comparison of two-point numerical differencing and analytic
solutions and the errors from the two-point method, run **geostrophicwind.py**.

To check the order of accuracy of the two-point difference wind calculation 
method and to plot a graph of the absolute error vs. the resolution, run
**checkorder.py**. 

To implement the more accurate differentiation method (3-point differencing)
and to plot a comparison with the two-point and analytic solutions as well as
errors of the three-point method, run **testaccuratediff.py**. 

To check the order of accuracy of the three-point difference wind calculation
method and to plot a graph of the absolute error vs. the resolution, run
**threepointcheckorder.py**.
