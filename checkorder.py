#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Code to check that the order of accuracy of our two-point numerical method 
is close to 2"""

import numpy as np 
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *
from orderofaccuracy import *

def checkorder():
    """Checks if the order of accuracy of our numerical method is close to 2"""
    
    N1 = 1000     #Number of intervals 1
    N2 = 10       #Number of intervals 2
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy1 = (ymax - ymin)/N1    #the first resolution
    dy2 = (ymax - ymin)/N2    #the second resolution
    
    #The spatial dimension y for two different resolutions
    y1 = np.linspace(ymin,ymax, N1+1)
    y2 = np.linspace(ymin,ymax, N2+1)
    
    #The pressure at the y points for both resolutions 
    p1 = pressure(y1, physProps)
    p2 = pressure(y2, physProps)
    
    #The exact geostrophic wind for both resolutions
    uExact1 = uGeoExact(y1, physProps)
    uExact2 = uGeoExact(y2, physProps)

    #The pressure gradient for both resolutions
    dpdy1 = gradient_2point(p1, dy1)
    dpdy2 = gradient_2point(p2, dy2)
    
    #The 2-point difference numerical wind for both resolutions
    u_2point1 = geoWind(dpdy1, physProps)
    u_2point2 = geoWind(dpdy2, physProps)
    
    #The errors for both resolutions 
    error1 = u_2point1 - uExact1
    error2 = u_2point2 - uExact2
    #print(error1)
    #print(error2)
    
    #Calculating average errors for both resolutions 
    avgerror1 = 0.0
    avgerror2 = 0.0
    
    for i in range(0, N1):
        avgerror1 += error1[i]
        
    avgerror1 /= (N1+1)
    
    for i in range(0, N2):
        avgerror2 += error2[i]
    
    avgerror2 /= (N2+1)
    
    #Print the average errors 
    print(avgerror1)
    print(avgerror2)
    
    #Order of accuracy n calculation 
    n = orderofaccuracy(dy1, dy2, avgerror1, avgerror2)
    print(n)
    
    #Plot error vs. resolution
    plotorder(dy1, dy2, avgerror1, avgerror2)
    
    
checkorder()
    
