#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Python code to numerically differentiate the pressure to calculate
the geostrophic wind  using 2-point differencing and to compare with the 
analytic solution graphically"""

import numpy as np 
import matplotlib.pyplot as plt 
from differentiate import *
from physProps import *

def geostrophicWind():
    """Calculates the geostrophic wind analytically and numerically and plots
    the two solutions"""
    
    #Resolution and size of the domain
    N = 10                  #the number of intervals to divide space into 
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N    #the length of each spacing 
    
    #The spatial dimension y 
    y = np.linspace(ymin,ymax, N+1)
    
    #The pressure at the y points and the exact geostrophic wind 
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)

    #The pressure gradient and wind using two-point differences 
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    #Graph to compare the numerical and analytic solutions
    font = {"size": 10}
    plt.rc("font", **font)

    #Plot the approximate and exact wind at y points 
    plt.plot(y/1000, uExact, "go", label = "Exact", ms = 10, markeredgewidth 
             = 1.5, markerfacecolor = "none")
    plt.plot(y/1000, u_2point, "bo", label = "Two-point differences")
    plt.legend(loc = "best")
    plt.xlabel("Vertical height y (in km)")
    plt.ylabel("Geostrophic wind u (in m/s)")
    plt.title("Numerical and Analytic Wind Solutions - A Comparison")
    plt.tight_layout()
    plt.savefig("twosolutions.pdf")
    plt.show()
    
    #Plot the errors
    error = u_2point - uExact
    plt.plot(y/1000, error, "r-", label = "Error")
    plt.xlabel("Vertical height y (in km)")
    plt.ylabel("Error in u (in m/s)")
    plt.legend()
    plt.axhline(linestyle = "-", color = "k")
    plt.title("Error between Numerical and Analytic Wind Solutions")
    plt.savefig("errors.pdf")
    plt.show()
    
if __name__ == "__main__":
    geostrophicWind()
