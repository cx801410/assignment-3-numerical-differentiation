#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Code with differentiation function to numerically evaluate the gradient of
    an input function using 2-point differences"""

import numpy as np 

#Functions for calculating gradients

def gradient_2point(f, dx):
    """Takes in the array of a function f evaluated at several x locations
    and a specified interval size dx. Returns the gradient of f at each 
    location dx apart using 2-point differences."""
        
    
    #Initialized the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two point differences at the end points 
    dfdx[0] = (f[1]-f[0])/dx
    dfdx[-1] = (f[-1]-f[-2])/dx
    
    #Centred differences for the midpoints 
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
        
    return dfdx