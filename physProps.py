#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Code containing a dictionary of physical properties and functions to 
evaluate pressure, analytical wind, and numerical wind at several vertical 
(y) locations"""

import numpy as np 

#A dictionary of physical properties 
physProps = {"pa" : 1e5,    #the mean pressure
             "pb" : 200.,   #the magnitude of the pressure variations
             "f" : 1e-4,    #the Coriolis parameter
             "rho" : 1.,    #density
             "L" : 2.4e6,   #length scale of the pressure variations (k)
             "ymin" : 0.,   #Start of the y domain
             "ymax" : 1e6,  #End of the y domain
             }

def pressure(y, props):
    """Takes in an array of y locations and a dictionary of properties and
    returns the pressure evaluated at those locations"""
    
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    return pa + pb*np.cos(y*np.pi/L)

def uGeoExact(y, props):
    """Takes in an array of y locations and a dictionary of properties and 
    returns the exact analytical geostrophic wind evaluated at those 
    locations"""
    
    pb = props["pb"]
    L = props["L"]
    rho = props["rho"]
    f = props["f"]
    return pb*np.pi/(rho*f*L)*np.sin(y*np.pi/L)

def geoWind(dpdy, props):
    """Takes in the gradient of a function evaluated at several 
    locations and a dictionary of properties and returns the numerical 
    geostrophic wind evaluated at those locations"""
    
    rho = props["rho"]
    f = props["f"]
    return -dpdy/(rho*f)