#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Code containing functions to calculate the order of accuracy of our 
numerical wind approximation method and to plot the absolute error as a 
function of resolution."""

import numpy as np 
import matplotlib.pyplot as plt

def orderofaccuracy(dy1, dy2, err1, err2):
    """Takes in two different resolutions dy1 and dy2 and two different average
    errors err1 and err2 of a numerical method and using these, returns the 
    order of convergence n of the method."""
    
    order = (np.log10(err1) - np.log10(err2))/(np.log10(dy1) - np.log10(dy2))
    return abs(order) 

def plotorder(dy1, dy2, err1, err2):
    """Takes in two resolutions and the two corresponding average errors and 
    plots the errors as a function of resolution in a log-log graph."""
    
    err1 = abs(err1)
    err2 = abs(err2)
    err = [err1, err2]
    dy = [dy1/1000, dy2/1000]
    plt.plot(dy, err, marker = ".", color = (0, 0.5, 0.5))
    plt.yscale("log")
    plt.xscale("log")
    plt.xlabel("Resolution (dy in km)")
    plt.ylabel("|Error|")
    plt.title("Absolute Error as a function of resolution")
    plt.savefig("logplot.pdf")
    plt.show() 
    

