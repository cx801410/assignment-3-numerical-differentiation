#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Python code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 3-point differences and compare both with 
the analytic solution and with the two-point difference method.
"""

import numpy as np 
import matplotlib.pyplot as plt 
from differentiate import *
from physProps import * 
from accuratediff import *

def moreaccurategeostrophicWind():
    """Calculates the geostrophic wind analytically and numerically and plots
    the two solutions. Also compares the numerical solutions obtained from
    two-point and three-point difference schemes."""
    
    #Resolution and size of the domain
    N = 10                  #the number of intervals to divide space into 
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin)/N    #the length of each spacing 
    
    #The spatial dimension y 
    y = np.linspace(ymin,ymax, N+1)
    
    #The pressure at the y points and the exact (analytic) geostrophic wind 
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)

    #The pressure gradient and wind using two-point differences 
    dpdy_2point = gradient_2point(p, dy)
    u_2point = geoWind(dpdy_2point, physProps)
    
    #The pressure gradient and wind using two-point differences 
    dpdy_3point = gradient_3point(p, dy)
    u_3point = geoWind(dpdy_3point, physProps)
    
    #Graphs to compare the two numerical solutions and analytic solutions
    font = {"size": 10}
    plt.rc("font", **font)

    #Plot the two- and three-point numerical and exact winds at the y points 
    plt.plot(y/1000, uExact, "go", label = "Exact", ms = 10, markeredgewidth 
             = 1.5, markerfacecolor = "none")
    plt.plot(y/1000, u_2point, "bo", label = "Two-point difference")
    plt.plot(y/1000, u_3point, "mo", label = "Three-point difference")
    plt.legend(loc = "best")
    plt.xlabel("Vertical height y (in km)")
    plt.ylabel("Geostrophic wind u (in m/s)")
    plt.title("2- and 3-point Numerical and Analytic Wind Solutions - A Comparison")
    plt.tight_layout()
    plt.savefig("threesolutions.pdf")
    plt.show()
    
    #Plot the errors from the two- and three-point numerical differencing
    error2 = u_2point - uExact
    error3 = u_3point - uExact
    plt.plot(y/1000, error2, "r-", label = "2-point Error")
    plt.plot(y/1000, error3, "c-", label = "3-point Error")
    plt.xlabel("Vertical height y (in km)")
    plt.ylabel("Error in u (in m/s)")
    plt.legend()
    plt.axhline(linestyle = "-", color = "k")
    plt.title("Errors from 2- and 3-point numerical wind solutions")
    plt.savefig("botherrors.pdf")
    plt.show()
    
moreaccurategeostrophicWind()