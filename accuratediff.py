#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Code containing a function to calculate the numerical gradient of an input
function using 3-point backward difference method."""

import numpy as np 

def gradient_3point(f, dx):
    """Takes in the array of a function f evaluated at several x locatons
    with specified interval size dx. Returns the gradient of f at each 
    location using 3-point backward differences."""
        
    #Initialized the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    
    #Two-point forward difference at the first point
    dfdx[0] = (f[1]-f[0])/dx
    
    #Two-point centered difference at the second point
    dfdx[1] = (f[2]-f[0])/(2*dx)
    
    #Three-point backward difference for all the other points 
    for i in range(2, len(f)):
        dfdx[i] = (f[i-2]-4*f[i-1]+3*f[i])/(2*dx)
        
    return dfdx